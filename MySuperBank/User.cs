﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySuperBank
{
    public class User
    {
        public string AccountNumber;
        public string Name;

        internal string AtmPin;
        public  decimal Balance
        {
            get
            {
                decimal balance = 0;

                foreach (var item in allTransactions)
                {
                    balance += item.Amount;
                }

                return balance;
            }
        }

        private List<Transaction> allTransactions = new List<Transaction>();
        public  void MakeDeposit(decimal amount, DateTime date, string note)
        {
            if (amount <= 0)
            {
               
                Console.WriteLine("Amount of deposit must be positive"); 
            }
            var deposit = new Transaction(amount, date, note);
            allTransactions.Add(deposit);
            Console.WriteLine(note);
        }

        public void MakeWithdrawal(decimal amount, DateTime date, string note)
        {
            while (amount <= 0)
            {
                Console.WriteLine( "Amount of withdrawal must be positive\nEnter Amount: ");
                amount = Convert.ToDecimal( Console.ReadLine());
            }
            if (Balance - amount < 0)
            {
                Console.WriteLine("Not sufficient funds for this withdrawal");
            }
            var withdrawal = new Transaction(-amount, date, note);
            allTransactions.Add(withdrawal);
            Console.WriteLine(note);
        }
    }
}

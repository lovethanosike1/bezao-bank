﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySuperBank
{
   public static class AccountService
    {    

        public static User _user;
        public static List<Users> allUsers = new List<Users>();


        public static void Register(BankAccount bankAccount)
        {
            _user = new User
            {
                AccountNumber = bankAccount.Number,
                Name = bankAccount.Owner,
                AtmPin = "0000"
                
            };

            var newUser = new Users(_user.AccountNumber, _user.AtmPin);

            var allUsers = new Dictionary<string, string>();
            allUsers.Add(newUser.AccountNumber, newUser.AtmPin);



            Console.WriteLine($"Welcome {_user.Name}, You now have an Account with Bezao Bank.\n Account Details:\n" +
               $"Name: {_user.Name.ToUpper()}\nAccount Number: {_user.AccountNumber}\nBalance: {_user.Balance} ");

            Console.WriteLine($"Will you like to make your first deposit? ");
            Console.WriteLine($"Press 1 for Yes OR Press 2 for NO");
            var input = Console.ReadLine();

            input = Helpers.RepeatInput(input, "YES", "NO");

            if (input == "1")
            {
                Console.WriteLine("How much do you want to deposit: ");
                var depositMoney = Convert.ToDecimal(Console.ReadLine());
                var depositTime = $"{DateTime.Now.Day}/{DateTime.Now.Month}/{DateTime.Now.Year} by {DateTime.Now.TimeOfDay.Hours}:{DateTime.Now.TimeOfDay.Minutes}";
                _user.MakeDeposit(depositMoney, DateTime.Now, $"Welldone, You made your first deposit of {depositMoney} on {depositTime}\n" +
                    $"Thanks for Banking with us ");

            }

            else
            {
                Console.WriteLine("Thanks for Banking with us, You can always deposit through the Bank or ATM");
            }
            

        }

        

       






    }
}

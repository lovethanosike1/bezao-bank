﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySuperBank
{
   public static class Helpers
    {


        public static string RepeatInput(string input, string firstInputFunction,string secondInputFunction)
        {
            while(input != "1" && input != "2"){
                Console.WriteLine($"Invallid Input!!!\n  {firstInputFunction}: Press 1 \n {secondInputFunction}: Press 2 ");
                input = Console.ReadLine();
            }

            return input;
        }


        public static string RepeatInput2(string input, string firstInputFunction, string secondInputFunction, string thirdInputFunction)
        {
            while (input != "1" && input != "2" && input!="3")
            {
                Console.WriteLine($"Invallid Input!!!\n  {firstInputFunction}: Press 1 \n " +
                    $"{secondInputFunction}: Press 2\n {thirdInputFunction}: Press 3 ");
                input = Console.ReadLine();
            }

            return input;
        }
    }


}

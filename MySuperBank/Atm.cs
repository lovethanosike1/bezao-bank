﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySuperBank
{   
   
    public class Atm 
    {

        //An ATM is a machine that helps users to withdraw/deposit/check balance
        //it needs a users account number and pin , from the card confirms the user
        //after confirmation, user can perform a transaction.
        //


        public string ChangeLanguage(string language)
        {
            if(language == "1")
            {
                return "English";
            }
            else if(language == "2")
            {
                return "Igbo";
            }

            else
            {
                return "false";
            }
                
            

                 
        }

        public bool ConfirmUser(string accountNumber, string pin)
        {


            if (AccountService._user.AccountNumber != accountNumber || AccountService._user.AtmPin != pin)

                return false;

            return true;
       
        }

        public void Deposit(decimal amount, DateTime dateTime, string note)
        {
            
            AccountService._user.MakeDeposit(amount, dateTime, $"{note}");

        }

        public void Withdraw(decimal amount, DateTime dateTime, string note)
        {
            AccountService._user.MakeWithdrawal(amount, dateTime, $"{note}");

        }

        public void CheckBalance(string note)
        {
            Console.WriteLine($"{note}: {AccountService._user.Balance}"); 
        }

        public string ChangePin(string prevPin, string newPin)
        {
            int result;
            if(int.TryParse(prevPin, out result) || int.TryParse(newPin, out result))
            {if (prevPin == AccountService._user.AtmPin)
                {
                    AccountService._user.AtmPin = newPin;
                    return AccountService._user.AtmPin;
                }
                return "0000";

            }

            else
            {
                return "Pin should be numbers";
            }
            
        }

        public bool ValidDenomination(decimal amount)
        {
            if(amount % 1000 == 0 || amount % 500 == 0)
                return true;
            return false;
        }

        public static void InitializePin(Atm atm)
        {
          

        }

    }
}
